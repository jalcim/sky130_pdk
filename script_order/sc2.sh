mkdir json spice gds svg data cdl lef tsv csv
mv *.json json/
mv *.data data/
mv *.spice spice/
mv *.gds gds/
mv *.svg svg/
mv *.data data/
mv *.cdl cdl/
mv *.lef lef/
mv *.tsv tsv/
mv *.csv csv/

rmdir *
