mkdir aM bM hcM mcM aF corner pm3
mv *aM* aM/
mv *bM* bM/
mv *hcM* hcM/
mv *mcM* mcM/
mv *aF* aF/
mv *corner* corner/
mv *pm3* pm3/

cd aM/
mkdir aM02W1p65L0p aM02W3p00L0p aM02W5p00L0p aM04W1p65L0p aM04W3p00L0p aM04W5p00L0p
mv *aM02W1p65L0p* aM02W1p65L0p/
mv *aM02W3p00L0p* aM02W3p00L0p/
mv *aM02W5p00L0p* aM02W5p00L0p/
mv *aM04W1p65L0p* aM04W1p65L0p/
mv *aM04W3p00L0p* aM04W3p00L0p/
mv *aM04W5p00L0p* aM04W5p00L0p/
rmdir *
cd ..

cd bM/
mkdir bM02W1p65L0p bM02W3p00L0p bM02W5p00L0p bM04W1p65L0p bM04W3p00L0p bM04W5p00L0p
mv *bM02W1p65L0p* bM02W1p65L0p/
mv *bM02W3p00L0p* bM02W3p00L0p/
mv *bM02W5p00L0p* bM02W5p00L0p/
mv *bM04W1p65L0p* bM04W1p65L0p/
mv *bM04W3p00L0p* bM04W3p00L0p/
mv *bM04W5p00L0p* bM04W5p00L0p/
rmdir *
cd ..

cd aF/
mkdir aF02 aF04 aF06 aF08
mv *aF02* aF02/
mv *aF04* aF04/
mv *aF06* aF06/
mv *aF08* aF08/
rmdir *
cd ..

rmdir *
