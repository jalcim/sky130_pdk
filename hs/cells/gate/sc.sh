mkdir json spice gds svg data cdl lef tsv csv verilog
mv *.json json/
mv *.data data/
mv *.spice spice/
mv *.gds gds/
mv *.svg svg/
mv *.cdl cdl/
mv *.lef lef/
mv *.tsv tsv/
mv *.csv csv/
mv *.v verilog/

rmdir *
