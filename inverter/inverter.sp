.option scale=1E-6

* Include SkyWater sky130 device models

.include "sky130_fd_pr__nfet_01v8__tt.pm3.spice"
.include "sky130_fd_pr__pfet_01v8__tt.pm3.spice"

.param sky130_fd_pr__nfet_01v8__toxe_slope=3.443e-03
.param sky130_fd_pr__nfet_01v8__lint_slope=0.0
.param sky130_fd_pr__nfet_01v8__nfactor_slope=0.0
.param sky130_fd_pr__nfet_01v8__voff_slope=0.007
.param sky130_fd_pr__nfet_01v8__vth0_slope=3.356e-03
.param sky130_fd_pr__nfet_01v8__vth0_slope1=7.356e-03
.param sky130_fd_pr__nfet_01v8__wint_slope=0

.param sky130_fd_pr__pfet_01v8__toxe_slope=4.443e-03
.param sky130_fd_pr__pfet_01v8__toxe_slope1=6.443e-03
.param sky130_fd_pr__pfet_01v8__nfactor_slope=0.1
.param sky130_fd_pr__pfet_01v8__nfactor_slope1=0.1
.param sky130_fd_pr__pfet_01v8__voff_slope=0.0
.param sky130_fd_pr__pfet_01v8__voff_slope1=0.0
.param sky130_fd_pr__pfet_01v8__vth0_slope=5.856e-03
.param sky130_fd_pr__pfet_01v8__vth0_slope1=7.356e-03

.option scale=1.0u

.param
+ sky130_fd_pr__pfet_01v8__lkvth0_diff = .0e-6
+ sky130_fd_pr__pfet_01v8__wlod_diff = .0e-6
+ sky130_fd_pr__pfet_01v8__lku0_diff = 0.0
+ sky130_fd_pr__pfet_01v8__kvsat_diff = 0.5
+ sky130_fd_pr__pfet_01v8__kvth0_diff = 3.29e-8
+ sky130_fd_pr__pfet_01v8__wkvth0_diff = .20e-6
+ sky130_fd_pr__pfet_01v8__ku0_diff = 4.5e-8
+ sky130_fd_pr__pfet_01v8__wku0_diff = .25e-6

Vin SOURCE 0 DC 1.8v
Vin1 GATE 0 pulse(0v 1.8v 0 0ns 0ns 20ns 40ns)

X1 3 GATE SOURCE SOURCE sky130_fd_pr__pfet_01v8 
X1 3 GATE 0 0 sky130_fd_pr__nfet_01v8 

.tran 1ns 60ns
.control
run
plot V(GATE)
plot V(3)

.endc
.end
